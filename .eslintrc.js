module.exports = {
  "extends": "standard-with-typescript",
  "parserOptions": {
      "project": "./tsconfig.json"
  },
  rules: {
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/no-misused-promises': 'off',
    '@typescript-eslint/prefer-readonly': 'off',
    '@typescript-eslint/require-await': 'off'
  }
}

