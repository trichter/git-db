# git-db
_A flat-file database based on git_

#### State
very early version

#### Example
```ts
import GitDB, { YAMLFile } from 'git-db'

const db = new GitDB('./repo')

const master = await db.checkoutBranch('master')
await master.loadData()

const files = master.get((file) => file instanceof YAMLFile)

```


