import { promises as fs } from 'fs'
import * as path from 'path'
import * as childProcess from 'child_process'
import { Gaze } from 'gaze'
import { EventEmitter } from 'events'

// const tracer = Tracer.colorConsole()
interface LogEntry {hash: string, message: string, date: string}

async function spawn (cmd: string, args?: string[], cwd?: string): Promise<string> {
  return new Promise((resolve, reject) => {
    const ps = childProcess.spawn(cmd, args, {
      cwd: cwd,
      env: {
        GIT_SSH_COMMAND: process.env.GIT_SSH_KEY ? `ssh -i ${process.env.GIT_SSH_KEY}` : undefined
      }
    })
    const stderr: Buffer[] = []
    const stdout: Buffer[] = []
    ps.stdout.on('data', d => stdout.push(d))
    ps.stderr.on('data', d => stderr.push(d))
    ps.on('close', (code) => {
      if (code !== 0) {
        const msg = Buffer.concat([...stdout, ...stderr]).toString('utf8')
        const err = new Error(msg.trim() || `Command exited with return code ${code}`)

        // @ts-ignore
        err.returnCode = code
        reject(err)
      } else {
        resolve(Buffer.concat(stdout).toString('utf8'))
      }
    })
  })
}

export class Branch extends EventEmitter {
  private watcher?: Gaze
  constructor (readonly git: ParallelGit, readonly branchName: string) {
    super()
  }

  public async runGitCommand (args: string[]): Promise<string> {
    return spawn('git', args, path.join(this.git.repoPath, this.branchName))
  }

  async fork (name: string): Promise<Branch> {
    const branchDir = path.join(this.git.repoPath, name)

    const res = await this.runGitCommand(['worktree', 'add', branchDir])
    if (res.match(/is now at /)) {
      return new Branch(this.git, name)
    } else {
      throw new Error(res)
    }
  }

  async readFile (file: string, encoding: string|null = null): Promise<string|Buffer> {
    return fs.readFile(path.join(this.git.repoPath, this.branchName, file), encoding)
  }

  async writeFile (file: string, content: string|Buffer, encoding: string|null = 'utf8'): Promise<void> {
    const directory = path.dirname(path.join(this.git.repoPath, this.branchName, file))
    try {
      await fs.stat(directory)
    } catch (err) {
      await fs.mkdir(directory, {
        recursive: true
      })
    }
    if (content instanceof Buffer) {
      return fs.writeFile(path.join(this.git.repoPath, this.branchName, file), content)
    } else {
      return fs.writeFile(path.join(this.git.repoPath, this.branchName, file), content, encoding)
    }
  }

  async deleteFile (file: string): Promise<void> {
    return fs.unlink(path.join(this.git.repoPath, this.branchName, file))
  }

  async readDir (dir: string): Promise<string[]> {
    return (await fs.readdir(path.join(this.git.repoPath, this.branchName, dir)))
      .filter(file => file !== '.git')
  }

  async getFileList (dir = ''): Promise<string[]> {
    const res = await spawn('find', '. -type f -not -path ./.git/*'.split(' '), path.join(this.git.repoPath, this.branchName, dir))
    if (!res.trim()) return []
    return res.trim().split('\n').map(filename => filename.replace(/^\.\//, ''))
  }

  async isDirectory (file: string): Promise<boolean> {
    const s = await fs.stat(path.join(this.git.repoPath, this.branchName, file))
    return s.isDirectory()
  }

  async add (path: string = '.'): Promise<void> {
    await this.runGitCommand(['add', path])
  }

  async commit (message: string): Promise<void> {
    try {
      await this.runGitCommand(['commit', '-m', message])
    } catch(err) {
      if(!err.message.match(/nothing to commit/)) {
        throw err
      }
    }
  }

  startWatcher (): void {
    const gaze = new Gaze('**/*', {
      interval: 1000,
      debounceDelay: 1000,
      cwd: path.join(this.git.repoPath, this.branchName)
    })
    this.watcher = gaze
    gaze.on('ready', (watcher: any) => {
      // console.log(watcher)
    })
    gaze.on('added', (filepath: string) => {
      const relative = path.relative(path.join(this.git.repoPath, this.branchName), filepath)
      this.emit('added', relative)
    })
    gaze.on('changed', (filepath: string) => {
      const relative = path.relative(path.join(this.git.repoPath, this.branchName), filepath)
      this.emit('changed', relative)
    })
    gaze.on('deleted', (filepath: string) => {
      const relative = path.relative(path.join(this.git.repoPath, this.branchName), filepath)
      this.emit('deleted', relative)
    })
    gaze.on('renamed', (newPath: string, oldPath: string) => {
      const relative = path.relative(path.join(this.git.repoPath, this.branchName), newPath)
      this.emit('added', relative)
    })

    gaze.on('error', (err: any) => {
      if (err.code === 'ENOENT') {
        const relative = path.relative(path.join(this.git.repoPath, this.branchName), err.filename)
        this.emit('deleted', relative)
        gaze.remove(err.filename)
      } else {
        console.log(err.code)
      }
    })
  }

  stopWatcher (): void {
    if (!this.watcher) return
    this.watcher.close()
  }

  async push (remote: string, branch: string): Promise<boolean> {
    const res = await this.runGitCommand(['pull', remote, branch])
    if (res.trim().match(/completed with|Everything up-to-date|Already up to date/)) {
      return true
    } else {
      throw new Error(res.trim())
    }
  }

  async pull (remote: string, branch: string): Promise<boolean> {
    const res = await this.runGitCommand(['pull', remote, branch])
    if (res.trim().match(/Already up to date|files changed/)) {
      return true
    } else {
      throw new Error(res.trim())
    }
  }

  async reset (ref: string): Promise<boolean> {
    const res = await this.runGitCommand(['reset', '--hard', ref])
    if (res.trim().match(/HEAD is now at/)) {
      return true
    } else {
      throw new Error(res.trim())
    }
  }

  // cleans all untracked files
  async clean (): Promise<void> {
    await this.runGitCommand(['clean', '-f', '-d'])
  }

  // async getForkPoint(withBranch: Branch): Promise<string> {
  //     return null
  // }
  // async getLog(): Promise<LogEntry[]> {
  //     return []
  // }
  // async getChangedFileList(): Promise<string[]> {
  //     return []
  // }
}

export default class ParallelGit {
  repoPath: string
  constructor (p: string) {
    this.repoPath = path.resolve(p)
  }

  async checkoutBranch (branchName: string): Promise<Branch> {
    if (!await this.branchExists(branchName)) {
      throw new Error(`could not find branch '${branchName}' as a subdirectory in '${this.repoPath}'`)
    }
    return new Branch(this, branchName)
  }

  async branchExists (branchName: string): Promise<boolean> {
    try {
      await fs.stat(path.join(this.repoPath, branchName))
      return true
    } catch (err) {
      return false
    }
  }
  // static async clone(url: string, path: string, masterBranch='master') {
  //     await spawn('git', ['clone', url, path.join(), ], )
  // }
  // async getBranches(branchName): Promise<Branch[]> {
  //     return []
  // }
}
