import ParallelGit, { Branch } from './ParallelGit'
import { BaseFile, YAMLFile, MarkdownFile, ImageFile } from './fileclasses'
import * as crypto from 'crypto'
import Logger from '@trichter/logger'
import 'colors'
export { BaseFile, YAMLFile, MarkdownFile, ImageFile }
const log = Logger('git-db')

function createHash (buf: Buffer): string {
  return crypto.createHash('sha1').update(buf).digest('hex')
}

export default class GitDB extends ParallelGit {
  private fileClasses: Array<typeof BaseFile> = [
    BaseFile,
    YAMLFile,
    MarkdownFile,
    ImageFile
  ]

  registerFileClass (fileClass: typeof BaseFile): void {
    log.debug(`register new file class ${fileClass.name}`)
    this.fileClasses.push(fileClass)
  }

  getFileClass (filename: string): typeof BaseFile {
    for (let i = this.fileClasses.length - 1; i >= 0; i--) {
      const c = this.fileClasses[i]
      if (c.filenameRegex && filename.match(c.filenameRegex)) return c
    }
    throw new Error(`could not find a file class for '${filename}'`)
  }

  async checkoutBranch (branchName: string): Promise<GitDBBranch> {
    await super.checkoutBranch(branchName)
    return new GitDBBranch(this, branchName)
  }
}

export class GitDBBranch extends Branch {
  private files: BaseFile[] = []
  private fileMap: {[filename: string]: BaseFile} = {}
  private isLoaded: boolean = false
  constructor (readonly db: GitDB, branch: string) {
    super(db, branch)
    this.on('added', async (path) => {
      try {
        if (await this.isDirectory(path)) {
          log.info(`[${this.branchName}] new directory detectd`, path)
          for (const file of await this.getFileList(path)) {
            await this.loadFile(file)
          }
        } else {
          log.info(`[${this.branchName}] new file detectd`, path)
          await this.loadFile(path)
        }
      } catch (err) {
        log.error(err)
      }
    })
    this.on('deleted', async (path) => {
      log.info(`[${this.branchName}] file removal detected`, path)
      if (this.fileMap[path]) {
        await this.removeFile(path)
      } else {
        for (const file of Object.keys(this.fileMap)) {
          // file in the given path?
          if (file.indexOf(path) === 0) {
            await this.removeFile(file)
          }
        }
      }
    })
    this.on('changed', (filename) => {
      log.info(`[${this.branchName}] file change detected`, filename)
      // this.loadFile(filename)
    })
  }

  async init (): Promise<void> {
    await this.loadData()
  }

  async loadData (): Promise<void> {
    // get list of all files
    const files = await this.getFileList()
    for (const filename of files) {
      await this.loadFile(filename)
    }
    this.isLoaded = true
  }

  private async loadFile (filename: string): Promise<void> {
    log.debug(`[${this.branchName}] load file ${filename}`)
    // get correct file class
    const C = this.db.getFileClass(filename)

    // create instance
    const file = new C(filename)

    // read content and parse it
    const content = await this.readFile(file.filename, undefined) as Buffer
    file._hashStored = createHash(content)
    file.parse(content)

    if (this.fileMap[filename]) {
      await this.removeFile(filename)
    }
    this.fileMap[filename] = file
    this.files.push(file)
  }

  private async removeFile (filename: string): Promise<void> {
    log.debug(`[${this.branchName}] remove file ${filename}`)
    if (!this.fileMap[filename]) {
      return
    }
    const index = this.files.indexOf(this.fileMap[filename])
    if (index === -1) {
      log.warn(`[${this.branchName}] ${filename} exists in fileMap, but not in the files array`)
      return
    }
    // remove from array
    this.files.splice(index, 1)
    delete this.fileMap[filename]
    this.emit('fileRemoved')
  }

  get (filter: (file: BaseFile) => boolean): BaseFile[] {
    if (!this.isLoaded) throw new Error('branch has not been loaded yet. use branch.loadData() before using data')
    return this.files.filter(filter)
  }

  getOne (filter: (file: BaseFile) => boolean): BaseFile|undefined {
    if (!this.isLoaded) throw new Error('branch has not been loaded consyet. use branch.loadData() before using data')
    return this.files.find(filter)
  }

  async save (file: BaseFile): Promise<void> {
    const data = file.toBuffer()
    if (file._hashStored && createHash(data) === file._hashStored) {
      // file did not change
      return
    }
    log.debug(`[${this.branchName}] store file ${file.filename.dim}`)
    await this.writeFile(file.filename, data)

    // (re)load the file
    await this.loadFile(file.filename)
  }

  async fork (name: string): Promise<GitDBBranch> {
    const branch = await super.fork(name)
    return new GitDBBranch(this.db, branch.branchName)
  }
}
