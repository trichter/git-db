import BaseFile from './BaseFile'
import * as yaml from 'yaml'
import * as Ajv from 'ajv'

const ajv = new Ajv({
  allErrors: true
})

export default class YAMLFile extends BaseFile {
  public static readonly filenameRegex: RegExp = /\.(yml|yaml)$/i
  public data: any = {}

  // @ts-ignore
  private validator: Ajv.ValidateFunction? = null

  protected useSchema (schema: any): void {
    this.validator = ajv.compile(schema)
  }

  parse (body: Buffer|string): void {
    this.data = yaml.parse(body.toString('utf-8'))
    if (this.validator) this.validate()
    if (this.data === null) this.data = {}
  }

  toString (): string {
    return yaml.stringify(this.data)
  }

  toBuffer (): Buffer {
    return Buffer.from(this.toString(), 'utf-8')
  }

  validate (): void {
    if (!this.validator) throw new Error('this method can only be used if a schema got added.')
    const valid = this.validator(this.data)
    if (!valid) {
      console.log(this.validator.errors)
      if (this.validator.errors) {
        throw new Error(`Error while reading ${this.filename}:\n${this.validator.errors.map(e => ` - [${Object.values(e.params)}] ${e.message}`).join('\n')}`)
      } else {
        throw new Error(`Error while reading ${this.filename}: unknown error`)
      }
    }
  }
}
