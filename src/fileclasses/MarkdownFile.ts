import YAMLFile from './YAMLFile'

export default class MarkdownFile extends YAMLFile {
  public static readonly filenameRegex: RegExp = /.*\.md/
  body: string = ''

  parse (buf: Buffer): void {
    const body = buf.toString('utf-8')
    // markdown with frontmatter?
    const fm = body.match(/^---\s*(\n[\s\S]+?|)\n---\s*\n{0,1}/m)
    if (fm) {
      super.parse(fm[1])
      this.body = body.slice(fm[0].length)
    } else {
      this.body = body
    }
  }

  toString (): string {
    // only add frontmatter, if this.data is not empty
    if (this.data && Object.keys(this.data)) {
      return `---\n${super.toString().trim()}\n---\n${this.body}`
    } else {
      return this.body
    }
  }
}
