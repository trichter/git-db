import BaseFile from './BaseFile'

export default class ImageFile extends BaseFile {
  public static readonly filenameRegex: RegExp = /\.(png|jpg)$/i

  get contentType (): string {
    throw new Error('unimplemented')
  }

  setImage (data: Buffer): void {
    this.content = data
  }
}
