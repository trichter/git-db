import * as path from 'path'
export default class BaseFile {
  public static readonly filenameRegex: RegExp = /.*/
  protected content: Buffer
  _hashStored: string = ''
  constructor (readonly filename: string) {
    this.content = Buffer.alloc(0)
  }

  parse (body: Buffer): void {
    this.content = body
  }

  toBuffer (): Buffer {
    return this.content
  }

  get directory (): string {
    return path.dirname(this.filename)
  }
}
