/* eslint-disable */
declare module 'gaze' {
  interface IOptions {
    interval?: number
    debounceDelay?: number
    mode?: string
    cwd?: string
  }
  export class Gaze {
    constructor(patterns: string|string[], opts?: IOptions, done?: (err: Error|null, watcher: any) => void)

    on(event: 'ready', cb: (watcher: Gaze) => void): Gaze
    on(event: 'all', cb: (event: any, filepath: string) => void): Gaze
    on(event: 'added', cb: (filepath: string) => void): Gaze
    on(event: 'changed', cb: (filepath: string) => void): Gaze
    on(event: 'deleted', cb: (filepath: string) => void): Gaze
    on(event: 'renamed', cb: (newPath: string, oldPath: string) => void): Gaze
    on(event: 'end', cb: () => void): Gaze
    on(event: 'error', cb: (err: Error) => void): Gaze
    on(event: 'nomatch', cb: () => void): Gaze

    close(): void
    add(patterns: any, callback: () => void): void
    remove(filepath: string): void
    watched(): any
    relative(dir: string, unixify: any): any
  }
}
